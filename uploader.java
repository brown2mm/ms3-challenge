	import java.io.FileReader;
	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.Statement;

	import au.com.bytecode.opencsv.CSV;
	import au.com.bytecode.opencsv.CSVReadProc.java;
	import au.com.bytecode.opencsv.CSVWriteProc.java;
	import au.com.bytecode.opencsv.CSVWriter.java;


	public class uploader
	{

		public static void main(String[] args)
		{
			readCsv();
			readCsvUsingLoad();

		}

		//method for reading and uploading
		private static void readCsv()
		{
			//read file andestablishes conntection with DB
			try (CSVReader reader = new CSVReader(new FileReader("upload.csv"), ',');
				Connection Connection = DBConnection.getConnection("jdbc:mysql://localhost:3306/challenge" +
                                   "user=root&password=MS3challenge");)
			{
				//get the insert type for the database
				String insertQuery = "Insert into upload (a,b,c,d,e,f,g,h,i,j) values (?,?,?,?,?,?,?,?,?,?)";
            	PreparedStatement pstmt = connection.prepareStatement("jdbc:mysql://localhost:3306/challenge" +
                                   "user=root&password=MS3challenge");
            	String[] rowData = null;
            	int i=0;
            	int count =0;
                while((rowData = reader.readNext()) != null)
                {
                	for (String data : rowData)
					{
						pstmt.setString((i % 3) + 1, data);

						//add row to be uploaded
						if(++i % 10==0)
						{	
							 pstmt.addBatch();
							 count++;
						}

						//upload when there are 10 rows to save processing power
						if (i % 1000 == 0)
						{
							pstmt.executeBatch();
						}
					}
				}
				System.out.println("Data Successfully Uploaded:\n");
				
				System.out.println(count + "records recieved");
			}
            catch (Exception e)
            {
                e.printStackTrace();

            }
        }

        private static void readCsvUsingLoad()
    	{
       		try (Connection connection = DBConnection.getConnection("jdbc:mysql://localhost:3306/challenge" +
                                   "user=root&password=MS3challenge"))
        	{
                String loadQuery = "LOAD DATA LOCAL INFILE '" + ".\\ms3Interview.csv" + "' INTO TABLE upload FIELDS TERMINATED BY ','" + " LINES TERMINATED BY '\n' (a,b,c,d,e,f,g,h,i,j) ";
                System.out.println(loadQuery);
                Statement stmt = connection.createStatement();
                stmt.execute(loadQuery);
        	}
        	catch (Exception e)
        	{
                e.printStackTrace();
        	}
    	}	
}